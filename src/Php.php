<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\DataPropertyInterface;
use Soong\Data\Property;

/**
 * Transformer to apply arbitrary PHP code to the incoming value. The PHP code
 * obtains the incoming value as $data.
 *
 * Example:
 *
 * @code
 * destination_number:
 *   class: Soong\Transformer\Php
 *   source: source_number
 *   php: 'return $data*3 + 58;'
 * @endcode
 *
 * @package Soong\Transformer
 */
class Php implements TransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
    {
      $data = $data->getValue();
      // "eval" here is just a misspelling of "evil".
      $result = eval($configuration['php']);
      // @todo: Don't hardcode specific DataPropertyInterface implementation.
      return new Property($result);
    }

}
